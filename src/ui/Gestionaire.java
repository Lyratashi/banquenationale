package ui;

import gestionaireErreurs.ExceptionOperation;
import java.util.ArrayList;
import composants.CompteBancaire;
import static gestionaireErreurs.Information.DEPOT_OK;
import static gestionaireErreurs.Information.RETRAIT_OK;
import static gestionaireErreurs.Information.VIREMENT_OK;
import operationsBancaires.Depot;
import operationsBancaires.OperationBancaire;
import operationsBancaires.Retrait;
import operationsBancaires.Virement;

public enum Gestionaire implements Data{
    
    BANQUE();
    public void nouveauCompte(CompteBancaire cb){LISTE_COMPTE_BANCAIRE.add(cb);}
    public ArrayList<CompteBancaire> getListeCompteBancaire(){return LISTE_COMPTE_BANCAIRE;}
    public void effectuerRetrait(CompteBancaire cb, float montant){
        try {
            OperationBancaire r = new Retrait(cb, montant);
            LISTE_OPERATION.add(r);
            RETRAIT_OK.information();
        } catch (ExceptionOperation ex){ex.showError();}
    }
    public void effectuerDepot(CompteBancaire cb, float montant){
        try {
            OperationBancaire d=new Depot(cb, montant);
            LISTE_OPERATION.add(d);
             DEPOT_OK.information();
        } catch (ExceptionOperation ex){ex.showError();}
    }
    public void effectuerVirement(CompteBancaire e, CompteBancaire r, float montant){
        try {
            OperationBancaire v = new Virement(e, r, montant);
            LISTE_OPERATION.add(v);
            VIREMENT_OK.information();
        } catch (ExceptionOperation ex){}
    }
    public ArrayList<OperationBancaire> getHistorique(CompteBancaire cb){
        ArrayList<OperationBancaire> historique=new ArrayList<>();
        LISTE_OPERATION.stream().filter((ob) -> (ob.concerner(cb))).forEach((ob) -> {historique.add(ob);});
        return historique;
    }
    
}
