package ui;

import composants.CompteBancaire;
import composants.CompteBancaire;
import java.util.ArrayList;
import operationsBancaires.OperationBancaire;

public interface Data {
    ArrayList<CompteBancaire> LISTE_COMPTE_BANCAIRE=new ArrayList<>();
    ArrayList<OperationBancaire> LISTE_OPERATION=new ArrayList<>();
    
}
