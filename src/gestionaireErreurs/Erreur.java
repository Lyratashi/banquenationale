package gestionaireErreurs;

import helmo.nhpack.NHPack;

/* NOTE: Merci Benjamin de m avoir fait penser aux enum effectivement c est plus clair ainsi */
public enum Erreur {
    ERROR_RETRAIT("Retrait Impossible"),
    ERROR_DEPOT("Depot impossible"),
    ERROR_VIREMENT("Virement impossible"),
    ERROR_SELECT("Pas de compte selectioné"),
    ERROR_REGISTRED("Deux comyes sont aux moins necessaire"),
    CB_NOT_FOUND("Pas de compte enregistré");
       
    private final String ERREUR;
    private Erreur(String message) {this.ERREUR = message;}
    public String getMessage() {return this.ERREUR;}
    public void showErreur(){NHPack.getInstance().showError("ERREUR", this.getMessage());}
 }

