package gestionaireErreurs;

import helmo.nhpack.NHPack;

public enum Information {
    
    DEPOT_OK("Depot pris en compte"),
    RETRAIT_OK("Retrait pris en compte"),
    VIREMENT_OK("Virement effectué");
    
    private final String INFOS;
    private Information(String message) {this.INFOS = message;}
    public String getMessage() {return this.INFOS;}
    public void information(){NHPack.getInstance().showInformation("Sucsess", this.getMessage());}
}
