package gestionaireErreurs;

public class ExceptionOperation extends Exception{
    final private Erreur ERREUR;
    
    public ExceptionOperation(Erreur erreur){
        super(erreur.getMessage());
        this.ERREUR=erreur;
    }
    public void showError(){ERREUR.showErreur();}
    
}
