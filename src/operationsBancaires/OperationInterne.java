package operationsBancaires;

import composants.CompteBancaire;

public abstract class OperationInterne extends OperationBancaire {
    private final CompteBancaire CB;
    
    public OperationInterne(CompteBancaire cb, float montant){
        super(montant);
        this.CB=cb;
    }
    public CompteBancaire getCompteBancaire(){ return this.CB;}
     @Override public boolean concerner(CompteBancaire cb){ return this.getCompteBancaire().getCompte().equals(cb.getCompte());}
    
}
