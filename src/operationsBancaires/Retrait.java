package operationsBancaires;

import composants.CompteBancaire;
import static gestionaireErreurs.Erreur.ERROR_RETRAIT;
import gestionaireErreurs.ExceptionOperation;

final public class Retrait extends OperationInterne {
    public Retrait(CompteBancaire cb, float montant) throws ExceptionOperation{
        super(cb, montant);
       if(!this.executer()) throw new ExceptionOperation(ERROR_RETRAIT);
    }
    @Override public String getResume(){return "<"+ this.getDate()+"> RETRAIT  "+this.getCompteBancaire()+"   -"+this.getMontant()+" EUR\n";}
    @Override public String toString(){return this.getResume();}
    @Override public boolean executer(){return this.getCompteBancaire().retirer(this.getMontant());}
}
