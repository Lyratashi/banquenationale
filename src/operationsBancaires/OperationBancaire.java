package operationsBancaires;

import composants.CompteBancaire;
import java.util.Date;

public abstract class OperationBancaire{
    private final Date DATE_OPERATION;
    private final float MONTANT;
    public OperationBancaire(float montant){
        this.DATE_OPERATION = new Date();
        this.MONTANT = montant;
    }
    public float getMontant(){return this.MONTANT;}
    public abstract boolean executer();
    public abstract String getResume();
    public String getDate(){
        java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( "dd/MM/yy hh:mm:ss" );
        return formater.format(DATE_OPERATION);
    }
    public abstract boolean concerner(CompteBancaire cb);
}