package operationsBancaires;

import composants.CompteBancaire;

public abstract class OperationExterne extends OperationBancaire {
    final private CompteBancaire EMETTEUR;
    final private CompteBancaire RECEPTEUR;
    
    public OperationExterne(CompteBancaire emetteur, CompteBancaire recepteur, float montant){
        super(montant);
        this.EMETTEUR=emetteur;
        this.RECEPTEUR=recepteur;       
    }
    public CompteBancaire getEmetteur(){return this.EMETTEUR;}
    public CompteBancaire getRecepteur(){return this.RECEPTEUR;}
     @Override public boolean concerner(CompteBancaire cb){ return this.getEmetteur().getCompte().equals(cb.getCompte())||this.getRecepteur().getCompte().equals(cb.getCompte());}
    
}
