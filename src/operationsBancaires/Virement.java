package operationsBancaires;

import composants.CompteBancaire;
import static gestionaireErreurs.Erreur.ERROR_VIREMENT;
import gestionaireErreurs.ExceptionOperation;

final public class  Virement extends OperationExterne{
    
    public Virement(CompteBancaire emetteur, CompteBancaire recepteur, float montant) throws ExceptionOperation{
        super(emetteur, recepteur, montant);
        /* NOTE: l'exception ici, ne sera jamais affiché, on affichera plutot l exception du retrait, car si
                 le compte Emetteur n'a pas assez d argent le virement ne pourra s"effectué
        */
        if(!this.executer()) throw new ExceptionOperation(ERROR_VIREMENT);
    }
    @Override public String getResume(){return "<"+ this.getDate()+"> VIREMENT  "+this.getEmetteur() +" vers "+this.getRecepteur()+"   "+this.getMontant()+"EUR\n";}
    @Override public String toString(){return this.getResume();}
    @Override public boolean executer(){
    /* NOTE : il est vrai que l on peu se faire un virement à sois même dans la vrai vie et mettre une communication, ici j'ai
              considéré que l programme ne le permettrais pas, aussi je trouve cette facon opti car si on arrive pas à faire le
              retrait on ne fera pas le depot et l exception sera de suite lancé, c est pour cela qu on n affichera pas l exception
              de virement mais du retrait, si ce n est pas clair je tenterais de me justifier à l oral
    */
        try {
            Retrait r=new Retrait(this.getEmetteur(), this.getMontant());
            Depot e=new Depot(this.getRecepteur(), this.getMontant());
            return true;
        } catch (ExceptionOperation ex){ex.showError();}
        return false;
    }
    
}
