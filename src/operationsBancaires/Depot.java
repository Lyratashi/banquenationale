package operationsBancaires;

import composants.CompteBancaire;
import static gestionaireErreurs.Erreur.ERROR_DEPOT;
import gestionaireErreurs.ExceptionOperation;

final public class Depot extends OperationInterne{
    
    public Depot(CompteBancaire cb, float montant) throws ExceptionOperation{
        super(cb, montant);
        if(!this.executer()) throw new ExceptionOperation(ERROR_DEPOT);
    }
    @Override public String getResume(){return "<"+ this.getDate()+"> DEPOT  "+this.getCompteBancaire()+"   +"+this.getMontant()+" EUR\n";}
    @Override public String toString(){return this.getResume();}
    @Override public boolean executer(){return this.getCompteBancaire().deposer(this.getMontant());}
}
