/**
 *  COMPTE BANCAIRE
 * Permet de cree un compte bancaire
 * 
 */
package composants;

public class CompteBancaire{
    /* NOTE: S incremente automatiquement a chaque nouveau compte, on pourrais le faire à l exterieur mais je trouve
             que ici cela a du sens car cela permet de "cacher" le mecanisme de calcul de creation d§un compte
    */
    private static int cptCompte=0; 
    final private  String COMPTE;
    private float solde;
    
    public CompteBancaire(){
        cptCompte++;
        this.solde = 0.0f;
        this.COMPTE=String.format("000 - %07d - %02d", cptCompte, cptCompte%97); 
    }
    public float getSolde(){return this.solde;}
    public boolean deposer(float montant){
        if(montant == 0) return false;
        this.solde+=montant;
        return true;
    }
    public boolean retirer(float montant){
        if(montant == 0)return false;
        if(this.getSolde()<Math.abs(montant)) return false;
        this.solde-=Math.abs(montant);
        return true;
    }
    public String getCompte(){return this.COMPTE;}
    @Override public String toString(){return this.getCompte();}
}
