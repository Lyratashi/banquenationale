package controllers;

import composants.CompteBancaire;
import static ui.Gestionaire.BANQUE;
import java.util.ArrayList;

public abstract class Controller{
    
    private ArrayList<CompteBancaire> svCompteBancaire;
 
    public ArrayList<CompteBancaire> getSelectedValues(){return this.svCompteBancaire;}
    public void setSelectedValues(ArrayList<CompteBancaire> listeCb){this.svCompteBancaire=listeCb;}
    public CompteBancaire compteCourant(){ return this.getSelectedValues().get(0);} 
    public boolean pasDeCompte(){return this.getSelectedValues().isEmpty();}
    public ArrayList<CompteBancaire> getListeCompteBancaire(){return BANQUE.getListeCompteBancaire();}
}
