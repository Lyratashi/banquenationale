package controllers;

import composants.CompteBancaire;
import static ui.Gestionaire.BANQUE;
import operationsBancaires.OperationBancaire;
import java.util.ArrayList;

public class HController extends Controller{
    private CompteBancaire cb;
    public HController(){this.cb=this.getListeCompteBancaire().get(0);}
    public CompteBancaire getCompteBancaire(){return this.cb;}
    public void setCompteBancaire(CompteBancaire cb){this.cb=cb;}
    @Override public String toString(){return ""+this.cb;}
    public ArrayList<OperationBancaire> getHistorique(){return BANQUE.getHistorique(this.cb);}
    public void update(){this.cb=this.getCompteBancaire();}
    public String getHeader(){return String.format("COMPTE %s  SOLDE: %.2f EUR", this.cb, this.cb.getSolde());}
}
