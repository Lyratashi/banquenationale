package controllers;

import composants.CompteBancaire;
import static ui.Gestionaire.BANQUE;
import helmo.nhpack.NHPack;

public class VController extends Controller{
    private float montantVirement;
    private CompteBancaire receveur;
    private CompteBancaire emetteur;
    
    public VController(){
        this.emetteur=BANQUE.getListeCompteBancaire().get(0);
        this.receveur=BANQUE.getListeCompteBancaire().get(1);
    }
    public CompteBancaire getReceveur(){return this.receveur;}
    public void setReceveur(CompteBancaire receveur){this.receveur=receveur;}
    public void setEmetteur(CompteBancaire emetteur){this.emetteur=emetteur;}
    public CompteBancaire getEmetteur(){return this.emetteur;}
    public void setMontantVirement(float mv){this.montantVirement=mv;}
    public float getMontantVirement(){return this.montantVirement;}
    public void faireVirement(){
        BANQUE.effectuerVirement(this.getEmetteur(), this.getReceveur(), this.getMontantVirement());
        NHPack.getInstance().closeWindow("frontend.virement.xml");
        NHPack.getInstance().refreshMainWindow();
    }   
}
