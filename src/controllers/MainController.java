package controllers;

import static gestionaireErreurs.Erreur.CB_NOT_FOUND;
import static gestionaireErreurs.Erreur.ERROR_SELECT;
import composants.CompteBancaire;
import gestionaireErreurs.Erreur;
import static ui.Gestionaire.BANQUE;
import helmo.nhpack.NHPack;

public class MainController extends Controller{
    private float montant;
  
    public MainController(){this.montant=0.0f;}
    public void nouveauCompte(){BANQUE.nouveauCompte(new CompteBancaire());}
    public float getMontant(){return this.montant;}
    public void setMontant(float montant){this.montant=montant;}
    public void faireDepot(){
        if(this.pasDeCompte()){ERROR_SELECT.showErreur();return;}
        BANQUE.effectuerDepot(this.compteCourant(), this.getMontant());
    }
    public void faireRetrait(){
        if(this.pasDeCompte())if(this.pasDeCompte()){ERROR_SELECT.showErreur();return;}
        BANQUE.effectuerRetrait(this.compteCourant(), this.montant);
    }
   public void virement() {
       if(this.getListeCompteBancaire().size()<2){Erreur.ERROR_REGISTRED.showErreur(); return;}
        VController vc = new VController();
        NHPack.getInstance().loadWindow("frontend.virement.xml", vc);
        NHPack.getInstance().showWindow("frontend.virement.xml");    
    }
    public void historique(){
        if(this.getListeCompteBancaire().isEmpty()){CB_NOT_FOUND.showErreur();return;}
        HController hc = new HController();
        NHPack.getInstance().loadWindow("frontend.historique.xml", hc);
        NHPack.getInstance().showWindow("frontend.historique.xml");
    }
}
